# Overview #
This is a basic boomer consumer configuration for my Mac. Very much work in progress.
Jump to [zsh](#zsh) & [yabai & skhd](#yabai&skhd)

## Zsh ##
- Add the following to /etc/zprofile

``` text
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
```

## Yabai & Skhd ##
- Jump to [installation](#installation), [KeyBindings](#keybindings) & [Common Bugs](#commonbugs)
- Two packages from [yabai](https://github.com/koekeishiya/yabai) & [skhd](https://github.com/koekeishiya/skhd)

### Installation ###
- Reboot computer and hold down `Command + r`
- Open terminal and run the following command

``` shell
csrutil enable --without debug --without fs --no-internal
```

- Restart computer (on restart you may have to allow drivers to mount automatically in System Preferences (e.g. Google Drive))
- Open KeyChain Access
- Create a new certificate with {Name: yabai-cert; Identity Type: Self-Signed Root; Certificate Type: Code Signing}
- Open terminal and run

``` shell
brew install koekeishiya/formulae/yabai --HEAD
codesign -fs 'yabai-cert' $(which yabai)
brew install koekeishiya/formulae/skhd
```

- Open System Preferences -> Security & Privacy -> Privacy -> Accessibility
- Add yabai and skhd (both should be in `usr/local/bin/`)
- In terminal run

``` shell
git clone https://username@gitlab.com/Ivan-Antonov/config.git
cp -R .config/yabai ~/.config/
cp -R .config/skhd ~/.config/
sudo yabai --install-sa
```

- Now start the yabai and skhd

``` shell
brew services start yabai
brew services start skhd
```

- Any changes in skhdrc automatically update meanwhile to add yabai changes restart Dock

``` shell
pkill Dock
```
- To enable emacs to tile add the following line to the emacs config

``` emacs-lisp
(menu-bar-mode t)
```

#### Optional ####
- Kill the desktop and finder

``` shell
defaults write com.apple.finder CreateDesktop false
killall Finder
```
- Kill finder indefinetely (& restart)

``` shell
osascript -e 'tell app "Finder" to quit'
osascript -e 'tell app "Finder" to run'
```

- Hide and restore Dock

``` shell
# Hide Dock
defaults write com.apple.dock autohide -bool true 
defaults write com.apple.dock autohide-delay -float 1000 
defaults write com.apple.dock launchanim -bool false 
killall Dock

# Restore Dock
defaults write com.apple.dock autohide -bool false 
defaults delete com.apple.dock autohide-delay 
defaults write com.apple.dock launchanim -bool true 
killall Dock
```

### KeyBindings ###
Keybindings in skhd for yabai.

The basic keybinding is `basic` = `lctrl + cmd`

* `basic w` - reload the yabai and skhd configuration

* `basic [h,k,j,l]` - move window focus

* `basic lalt [h,k,j,l]` - swap window with window in given direction

* `basic [u,i,o,p]` - increase window size in given direction

* `basic lalt [u,i,o,p]` - decrease size in given direction

* `basic r` - rotate windows clockwise by 90 degrees

* `basic z` - mirror windows in y axis

* `basic x` - mirror windows in x axis

* `basic v` - toggle window fullscreen (temporarily)

* `basic f` - make window float ontop

* `basic [arrows up, left, right]` - make floating window fill top, left and right respectively

* `basic [c,d]` - create and destory desktops (workspaces) respectively

* `basic [1-9]` - focus on given desktop

* `basic lalt [1-9]` - move window to given desktop and switch focus

* `basic [-,=]` - switch to previous/ next monitor

* `basic [[,]]` - move window to given monitor and switch focus

### Common Bugs ###
- Run the following set of commands
- If the follwoing output is seen, change your `umask` to 0000

``` shell
$ sudo yabai --uninstall-sa; echo "Status: $?"
Status: 0

$ sudo yabai --install-sa; echo "Status: $?"
Status: 0

$ yabai --check-sa; echo "Status: $?"
Status: 1

$ sudo yabai --check-sa; echo "Status: $?"
Status: 0

$ yabai --load-sa; echo "Status: $?"
Status: 1

$ sudo yabai --load-sa; echo "Status: $?"
yabai: scripting-addition could not load, make sure SIP is disabled!
Status: 1
```

