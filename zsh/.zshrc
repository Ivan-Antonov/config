# Enable colors and change prompt:
autoload -U colors && colors

PS1="%B%{$fg[yellow]%}%n%{$fg[grey]%}@%{$fg[blue]%}%M%{$fg[grey]%}||%{$fg[magenta]%}(%~)%{$fg[red]%}[%?] %{$reset_color%}$%b "

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

echo -ne '\e[3 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[3 q' ;} # Use beam shape cursor for each new prompt.

# bind ranger key & switch directories when exit
alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
bindkey -s '^w' 'ranger\n'

# open man pages
function man-pdf() { man -t "$@" | open -f -a "Skim" ;}
bindkey -s '^n' 'man-pdf '

# open file nvim
bindkey -s '^v' 'nvim $(fzf)\n'

# python environments
function venv() { source ~/.venv/$@/bin/activate ;}
bindkey -s '^p' 'venv '

#rust up binding
fpath+=~/.zfunc

# Load zsh-syntax-highlighting; should be last
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh" || true
